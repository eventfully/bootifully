package org.eventfully.bootifully
import org.apache.camel.CamelContext
import org.apache.camel.ConsumerTemplate
import org.apache.camel.Exchange
import org.apache.camel.ProducerTemplate
import org.eventfully.bootifully.config.CamelConfig
import org.junit.After
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Unroll

@ContextConfiguration( classes = [ CamelConfig ] )
@DirtiesContext
class RabbitSpec extends Specification {

	@Autowired
	CamelContext camelContext

	@Autowired
	@Qualifier("rabbitMqBaseURI")
	String rabbitMqBaseURI

	@Autowired
	@Qualifier("rabbitMqUserPassword")
	String rabbitMqUserPassword

	ProducerTemplate producer
	ConsumerTemplate consumer

	String toUri
	String fromUri

	@Before
	public void setup() {

		producer = camelContext.createProducerTemplate()
		consumer = camelContext.createConsumerTemplate()
	}

	@After
	public void tearDown() {
	}


    @Unroll
	def "Sending '#payload' and receiving '#expected'"() {

		given: "A to URI"
		toUri = "$rabbitMqBaseURI/$exchange?exchangeType=topic&autoDelete=false&$rabbitMqUserPassword&routingKey=$toRouteKey"

		and: "A from URI"
		fromUri = "$rabbitMqBaseURI/$exchange?exchangeType=topic&autoDelete=false&$rabbitMqUserPassword&routingKey=$fromRouteKey&queue=outbound$queueSuffix"

		when: "The request is sent"
		producer.sendBody(toUri, payload)
		Exchange ex = consumer.receive(fromUri, 3000)
		String response = ex?.getIn()?.getBody(String.class)

		then: "A reply is received"
		(response)
		response == expected

		where:
		payload | expected 	| exchange  | toRouteKey 		| fromRouteKey		| queueSuffix
		"Hello" | "OLLEH" 	| "demoBox" | 'to.upper.do' 	| 'to.upper.done'	| 'UR'
		"Hej"	| "jeh" 	| "demoBox" | 'to.lower.do' 	| 'to.lower.done'	| 'LR'
	}
}
