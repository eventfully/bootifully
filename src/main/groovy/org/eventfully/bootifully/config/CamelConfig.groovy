package org.eventfully.bootifully.config

import org.apache.camel.CamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.spring.javaconfig.CamelConfiguration
import org.springframework.boot.autoconfigure.AutoConfigurationPackages.BasePackages;
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration

import com.rabbitmq.client.ConnectionFactory

@Configuration
@ComponentScan(basePackages = "org.eventfully.bootifully")
class CamelConfig extends CamelConfiguration {

	@Bean
	String rabbitMqBaseURI(){
		'rabbitmq://localhost:5672'
	}

	@Bean
	String rabbitMqUserPassword(){
		'username=guest&password=guest'
	}

	/**
	 * Custom connection factory is not available in endpoints until Camel 2.14.0 (current SNAPSHOT)
	 * ?connectionFactory=#rabbitCF()
	 * @return
	 */
	@Bean
	public ConnectionFactory rabbitCF(){
		def cf = new ConnectionFactory(host: "localhost", port: 5672, username: "guest", password: "guest");
	}
}