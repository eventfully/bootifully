package org.eventfully.bootifully.beans

import org.springframework.stereotype.Component
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Component
class UpperAndLowerCaseReverser {

	Logger logger = LoggerFactory.getLogger(UpperAndLowerCaseReverser.class)

	/**
	 * 
	 * @param payload
	 * @return payload with all lowercase and characters reversed
	 */
	String toReversedLower(String payload){
		payload.toLowerCase().reverse()
	}

	/**
	 * 
	 * @param payload
	 * @return payload with all uppercase and characters reversed
	 */
	String toReversedUpper(String payload){
		payload.toUpperCase().reverse()
	}
}
