package org.eventfully.bootifully.routes
import org.apache.camel.Exchange
import org.apache.camel.Message
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.rabbitmq.RabbitMQConstants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Component
class SimpleRabbitMQRoute extends RouteBuilder {

	static final String ROUTE_ID = "simpleRabbitMQRoute"
	
	@Autowired
	@Qualifier("rabbitMqBaseURI")
	String rabbitMqBaseURI
	
	@Autowired
	@Qualifier("rabbitMqUserPassword")
	String rabbitMqUserPassword

	@Override
	public void configure() throws Exception {
		from("direct:rabbitHole").routeId(ROUTE_ID)
				.process { Exchange exchange ->
					Message msg = exchange.in
					//	msg.setBody("Hello from the Rabbit hole!",  String.class)
					msg.setHeader(RabbitMQConstants.ROUTING_KEY, "sample1")
					msg.setHeader(RabbitMQConstants.DELIVERY_MODE, 2)
				}.to("${rabbitMqBaseURI}/tutorials?autoDelete=true&${rabbitMqUserPassword}")
	}
}
