package org.eventfully.bootifully.routes

import org.apache.camel.Message
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.CamelContext
import org.apache.camel.builder.RouteBuilder
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component
/**
 * Contains two routes that uses a bean for
 * - uppercase and reverse payload
 * - lowercase and reverse payload
 * They are referencing a bean to do the work even if
 * it could have just as easily be done with a 
 * transform. 
 *
 */
@Component
class UpperAndLowerCaseReverserRoute extends RouteBuilder {

	@Autowired
	@Qualifier("rabbitMqBaseURI")
	String rabbitMqBaseURI

	@Autowired
	@Qualifier("rabbitMqUserPassword")
	String rabbitMqUserPassword

	private static final String LOG_URI = 'log:org.eventfully.bootifully.routes?showAll=true&multiline=true'

	private static final String ALL_HEADERS = '*'

	@Override
	public void configure() throws Exception {

		from("${rabbitMqBaseURI}/demoBox?exchangeType=topic&autoDelete=false&routingKey=to.lower.do&${rabbitMqUserPassword}&queue=inboundLR")
				.routeId("toReversedLowerRoute")
				.tracing()
				.beanRef("upperAndLowerCaseReverser", "toReversedLower")
				.removeHeaders(ALL_HEADERS)
				.to("${rabbitMqBaseURI}/demoBox?exchangeType=topic&autoDelete=false&routingKey=to.lower.done&${rabbitMqUserPassword}")

		from("${rabbitMqBaseURI}/demoBox?exchangeType=topic&autoDelete=false&routingKey=to.upper.do&${rabbitMqUserPassword}&queue=inboundUR")
				.routeId("toReversedUpperRoute")
				.tracing()
				.beanRef("upperAndLowerCaseReverser", "toReversedUpper")
				.removeHeaders(ALL_HEADERS)
				.to("${rabbitMqBaseURI}/demoBox?exchangeType=topic&autoDelete=false&routingKey=to.upper.done&${rabbitMqUserPassword}")
	}
}
