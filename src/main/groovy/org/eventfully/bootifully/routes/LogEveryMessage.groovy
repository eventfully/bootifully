package org.eventfully.bootifully.routes

import org.apache.camel.builder.RouteBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Component
class LogEveryMessage extends RouteBuilder {

	@Autowired
	@Qualifier("rabbitMqBaseURI")
	String rabbitMqBaseURI
	
	@Autowired
	@Qualifier("rabbitMqUserPassword")
	String rabbitMqUserPassword
	
	private static final String ROUTE_ID = "LogEveryMessage"

	private static final String LOG_URI = "log:org.eventfully.bootifully.routes?showAll=true&multiline=true"

	@Override
	public void configure() throws Exception {
		from("${rabbitMqBaseURI}/demoBox?exchangeType=topic&autoDelete=false&routingKey=#&queue=logCommands&${rabbitMqUserPassword}")
				.routeId(ROUTE_ID)
				.to(LOG_URI)
	}
}
