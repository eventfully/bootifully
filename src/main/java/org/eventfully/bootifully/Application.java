package org.eventfully.bootifully;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application {

	public static void main(String[] args) {

		new SpringApplicationBuilder()
				.sources(new ClassPathResource("applicationContext.groovy"))
				.sources(Application.class).run(args);
	}
}
